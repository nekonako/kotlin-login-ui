package com.example.myapptests

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.widget.EditText
import android.widget.Button
import android.widget.Toast
import android.widget.TextView
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.example.myapptests.api.ServiceApi
import com.example.myapptests.api.UserApi
import com.example.myapptests.models.SigninRequest
import com.example.myapptests.models.SiginResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_login)

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)

        val mail = findViewById<ImageView>(R.id.mail)
        val lock = findViewById<ImageView>(R.id.lock)

        val usernameMessage = findViewById<TextView>(R.id.usernameMessage)
        val passwordMessage = findViewById<TextView>(R.id.passwordMessage)

        val passwordVisibility = findViewById<ImageView>(R.id.passwordVisibility)
        val forgot = findViewById<TextView>(R.id.forgot)

        val loginButton = findViewById<Button>(R.id.login)

        val editTexts = listOf<EditText>(username, password)

        // looping every EditText to TextWatcher
        for (editText in editTexts) {
            with(editText) {
                addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        val usernameString = username.text.toString().trim()
                        val passwordString = password.text.toString().trim()

                        if (!usernameString.isEmpty() && !passwordString.isEmpty()) {
                            loginButton.isEnabled = true
                            loginButton.setBackgroundColor(
                                ContextCompat.getColor(
                                    this@LoginActivity,
                                    R.color.red
                                )
                            )
                        } else {
                            loginButton.isEnabled = false
                            loginButton.setBackgroundColor(
                                ContextCompat.getColor(
                                    this@LoginActivity,
                                    R.color.grey
                                )
                            )
                        }
                    }

                    override fun afterTextChanged(s: Editable?) {}
                })
            }
        }

        // set password to visible or invisible
        passwordVisibility.setOnClickListener {
            if (password.transformationMethod == PasswordTransformationMethod.getInstance()) {
                password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                password.setSelection(password.length())
                passwordVisibility.setImageResource(R.drawable.ic_eye_error)
                forgot.setTextColor(ContextCompat.getColor(this, R.color.red))
            } else {
                password.transformationMethod = PasswordTransformationMethod.getInstance()
                password.setSelection(password.length())
                passwordVisibility.setImageResource(R.drawable.ic_eye_off)
                forgot.setTextColor(ContextCompat.getColor(this, R.color.white))
            }
        }

        // login button click listener
        loginButton.setOnClickListener {
            val usernameString = username.text.toString()
            val passwordString = password.text.toString()
            val userData = SigninRequest(usernameString, passwordString)
            val retrofit = ServiceApi.buildService(UserApi::class.java)
            val intent = Intent(this, DashboardActivity::class.java)
            retrofit.login(userData).enqueue(
                object : Callback<SiginResponse> {
                    override fun onFailure(call: Call<SiginResponse>, t: Throwable) {
                        Log.e("Error", t.message.toString())
                        Toast.makeText(
                            this@LoginActivity,
                            "Error",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onResponse(
                        call: Call<SiginResponse>,
                        response: Response<SiginResponse>
                    ) {
                        when (response.body()?.on) {
                            "" -> {
                                Toast.makeText(
                                    this@LoginActivity,
                                    "Selamat datang",
                                    Toast.LENGTH_SHORT
                                ).show()
                                startActivity(intent)
                            }
                            "username" -> {
                                Toast.makeText(
                                    this@LoginActivity,
                                    "username salah",
                                    Toast.LENGTH_SHORT
                                ).show()
                                usernameMessage.setText("Username/Email salah")
                                username.setBackgroundResource(R.drawable.border_shape_error)
                                mail.setImageResource(R.drawable.ic_mail_error)
                            }
                            "password" -> {
                                Toast.makeText(
                                    this@LoginActivity,
                                    "password salah",
                                    Toast.LENGTH_SHORT
                                ).show()
                                passwordMessage.setText("Password invalid")
                                password.setBackgroundResource(R.drawable.border_shape_error)
                                lock.setImageResource(R.drawable.ic_lock_error)
                            }
                            "all" -> {
                                Toast.makeText(
                                    this@LoginActivity,
                                    "username atau password salah",
                                    Toast.LENGTH_SHORT
                                ).show()
                                usernameMessage.setText("Username/Email salah")
                                passwordMessage.setText("Password invalid")
                                username.setBackgroundResource(R.drawable.border_shape_error)
                                mail.setImageResource(R.drawable.ic_mail_error)
                                password.setBackgroundResource(R.drawable.border_shape_error)
                                lock.setImageResource(R.drawable.ic_lock_error)
                            }
                        }
                    }
                }
            )
        }
    }
}