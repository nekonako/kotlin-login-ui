package com.example.myapptests

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class DashboardActivity : AppCompatActivity() {

    private lateinit var toolbar : Toolbar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        // set default action bar title to dashboard
        val actionBarTitle = findViewById<TextView>(R.id.action_bar_title)
        actionBarTitle.setText(R.string.dashboard)

        val homeFragment = HomeFragment()
        val messageFragment = MessageFragment()
        val profileFragment = ProfileFragment()
       setCurrentFragment(homeFragment)
        val bottomNavigation = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottomNavigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.home -> {
                    actionBarTitle.setText(R.string.dashboard)
                    setCurrentFragment(homeFragment)
                }
                R.id.message ->{
                    actionBarTitle.setText(R.string.chat)
                    setCurrentFragment(messageFragment)
                }
                R.id.profile -> {
                    actionBarTitle.setText(R.string.profile)
                    setCurrentFragment(profileFragment)
                }
            }
            true
        }
        toolbar = findViewById(R.id.action_bar)
        setSupportActionBar(toolbar)
    }

    private fun setCurrentFragment(fragment: Fragment) = supportFragmentManager.beginTransaction().apply {
        replace(R.id.fl_container, fragment)
        commit()
    }

}