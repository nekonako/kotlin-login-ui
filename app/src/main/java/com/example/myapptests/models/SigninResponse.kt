package com.example.myapptests.models

data class SiginResponse(
    val error: Int,
    val on: String,
    val message: String
)