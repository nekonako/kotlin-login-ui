package com.example.myapptests

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.fragment.app.Fragment

class ProfileFragment : Fragment(R.layout.fragment_profile) {
    private val logout = arrayOf("Logout")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentView = inflater.inflate(R.layout.fragment_profile, container, false)
        val profileList = resources.getStringArray(R.array.profile_array)
        val profileListView = fragmentView.findViewById<ListView>(R.id.profileListView)
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.list_view, R.id.label, profileList)
        profileListView.adapter = arrayAdapter
        val logoutView = fragmentView.findViewById<ListView>(R.id.logout)
        val arrayAdapter2 = ArrayAdapter(requireContext(), R.layout.logout_list_view, R.id.label, logout)
        logoutView.adapter = arrayAdapter2
        return fragmentView
    }
}