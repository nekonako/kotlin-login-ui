package com.example.myapptests.api

import com.example.myapptests.models.SigninRequest
import com.example.myapptests.models.SiginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface UserApi {
    @Headers("Content-Type: application/json")
    @POST("login")
    fun login(@Body userData: SigninRequest): Call<SiginResponse>
}