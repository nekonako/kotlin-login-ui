package com.example.myapptests

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.NumberFormat
import kotlin.collections.ArrayList


class  AxisDateFormatter(private val mValues: Array<String>): ValueFormatter() {
    override fun getFormattedValue(value: Float): String {
        return if(value >= 0){
            if(mValues.size > value.toInt()){
                mValues[value.toInt()]
            } else ""
        } else ""
    }
}

class HomeFragment : Fragment(R.layout.fragment_home) {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentView : View = inflater.inflate(R.layout.fragment_home, container, false)

        // dropdown item
        val spinnerPopulation = fragmentView.findViewById<Spinner>(R.id.spinner_dashboard)
        spinnerPopulation?.adapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.spinner_dashboard,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_item)
        }

        val spinnerMachine = fragmentView.findViewById<Spinner>(R.id.machine_population_spinner)
        spinnerMachine?.adapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.machine_population_spinner,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_item)
        }

        /*
        // PIE CHART
        */
        val pieChart = fragmentView.findViewById<PieChart>(R.id.pieChart)

        val dataMachines = ArrayList<PieEntry>()
        dataMachines.add(PieEntry(20201f, "ATM"))
        dataMachines.add(PieEntry(3329f, "CRM"))
        dataMachines.add(PieEntry(14257f, "CDM"))

        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#dc241f"))
        colors.add(Color.parseColor("#ffb443"))
        colors.add(Color.parseColor("#780000"))

        val dataSet = PieDataSet(dataMachines, "")

        dataSet.valueFormatter = PercentFormatter()
        dataSet.colors = colors
        dataSet.valueTextSize = 0F

        var totalMachine = 0
        for(i in dataMachines){
            totalMachine += + i.value.toInt()
        }

        val totalMachineText = fragmentView.findViewById<TextView>(R.id.totalMachine)
        totalMachineText.text = NumberFormat.getInstance().format(totalMachine).toString()

        pieChart.data = PieData(dataSet)
        pieChart.setDrawCenterText(true);
        //pieChart.centerText = "${NumberFormat.getNumberInstance(Locale.US).format(totalMachine)} Machines"
        pieChart.setCenterTextSize(16F)
        pieChart.description.text = ""
        pieChart.setTouchEnabled(true)
        pieChart.setDrawEntryLabels(false)
        pieChart.setUsePercentValues(true)
        pieChart.isRotationEnabled = false
        pieChart.setDrawEntryLabels(false)
        pieChart.legend.isEnabled = false
        pieChart.animateY(1400, Easing.EaseInOutQuad)
        pieChart.holeRadius = 70f
        pieChart.transparentCircleRadius = 0F
        pieChart.isDrawHoleEnabled = true
        pieChart.setHoleColor(Color.WHITE)

        pieChart.invalidate()

        /*
        // LINE CHART
         */
        val lineChart = fragmentView.findViewById<LineChart>(R.id.lineChart)
        lineChart.setTouchEnabled(true)
        lineChart.setPinchZoom(true)

        // date
        val date = ArrayList<String>()
        date.add("Jan")
        date.add("Feb")
        date.add("Mar")
        date.add("Apr")
        date.add("Mei")
        date.add("Jun")
        date.add("Jul")
        date.add("Agu")
        date.add("Sep")
        date.add("Okt")
        date.add("Nov")
        date.add("Des")

        // add line chart data
        val lineChartDataEntry = ArrayList<Entry>()
        lineChartDataEntry.add(Entry(0F,38000000F))
        lineChartDataEntry.add(Entry(1F,35000000F))
        lineChartDataEntry.add(Entry(2F,28000000F))
        lineChartDataEntry.add(Entry(3F,30000000F))
        lineChartDataEntry.add(Entry(4F,28000000F))
        lineChartDataEntry.add(Entry(5F,35000000F))
        lineChartDataEntry.add(Entry(6F,30000000F))
        lineChartDataEntry.add(Entry(7F,32000000F))
        lineChartDataEntry.add(Entry(8F,30000000F))
        lineChartDataEntry.add(Entry(9F,27000000F))
        lineChartDataEntry.add(Entry(10F,30000000F))
        lineChartDataEntry.add(Entry(11F,25000000F))

        val month = AxisDateFormatter(date.toArray(arrayOfNulls<String>(date.size)))
        lineChart.xAxis?.valueFormatter = month

        val lineChartDataSet = LineDataSet(lineChartDataEntry, "")
        lineChartDataSet.color = Color.parseColor("#dc241f")
        lineChartDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        lineChartDataSet.setCircleColor(Color.parseColor("#dc241f"))
        lineChartDataSet.setDrawCircleHole(false)
        lineChartDataSet.setDrawCircles(false)
        lineChartDataSet.lineWidth = 2F
        lineChartDataSet.setDrawValues(false)

        // gradient
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
            lineChartDataSet.setDrawFilled(true)
            val fillGradient = ContextCompat.getDrawable(requireContext(), R.drawable.gradient_red)
            lineChartDataSet.fillDrawable = fillGradient
        }

        lineChart.data = LineData(lineChartDataSet)
        lineChart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        lineChart.animateY(1000, Easing.EaseInOutQuad)
        lineChart.axisLeft.setDrawGridLines(false)
        lineChart.xAxis.setDrawGridLines(false)
        lineChart.xAxis.labelCount = lineChartDataEntry.size - 1
        lineChart.axisRight.isEnabled = false
        lineChart.legend.isEnabled = false

        val atmPerModelChart = fragmentView.findViewById<PieChart>(R.id.atmPerModelChart)

        val atmPerModelChartEntry = ArrayList<PieEntry>()
        atmPerModelChartEntry.add(PieEntry(14257F, "Model Name 1"))
        atmPerModelChartEntry.add(PieEntry(20201F, "Model Name 2"))
        atmPerModelChartEntry.add(PieEntry(3329F, "Model Name 3"))
        atmPerModelChartEntry.add(PieEntry(14257F, "Model Name 4"))
        atmPerModelChartEntry.add(PieEntry(14257F, "Model Name 5"))
        atmPerModelChartEntry.add(PieEntry(14257F, "Model Name 6"))
        atmPerModelChartEntry.add(PieEntry(14257F, "Model Name 7"))

        val atmPerModelChartColors : ArrayList<Int> = ArrayList()
        atmPerModelChartColors.add(ContextCompat.getColor(requireContext(),R.color.dark_red))
        atmPerModelChartColors.add(ContextCompat.getColor(requireContext(),R.color.red))
        atmPerModelChartColors.add(ContextCompat.getColor(requireContext(),R.color.yellow))
        atmPerModelChartColors.add(ContextCompat.getColor(requireContext(),R.color.light_yellow))
        atmPerModelChartColors.add(ContextCompat.getColor(requireContext(),R.color.blue))
        atmPerModelChartColors.add(ContextCompat.getColor(requireContext(),R.color.light_purple))
        atmPerModelChartColors.add(ContextCompat.getColor(requireContext(),R.color.purple))

        val atmPerModelDataSet = PieDataSet(atmPerModelChartEntry, "")
        atmPerModelDataSet.colors = atmPerModelChartColors
        atmPerModelDataSet.valueTextSize = 0F

        var totalAtmPerModel = 0
        for(i in atmPerModelChartEntry){
            totalAtmPerModel += i.value.toInt()
        }
        val totalAtmPerModelText = fragmentView.findViewById<TextView>(R.id.totalAtmPerModel)
        totalAtmPerModelText.text = NumberFormat.getInstance().format(totalAtmPerModel).toString()

        atmPerModelChart.data = PieData(atmPerModelDataSet)
        atmPerModelChart.legend.isEnabled = false
        atmPerModelChart.setDrawEntryLabels(false)
        atmPerModelChart.setUsePercentValues(false)
        atmPerModelChart.description.text = ""
        atmPerModelChart.isRotationEnabled = false
        atmPerModelChart.transparentCircleRadius = 0F
        atmPerModelChart.holeRadius = 70F

        return  fragmentView
    }
}